'use strict';

// scrolling
function anim(duration) {
  var temp;
  return function(sel) {
    cancelAnimationFrame(temp);
    var start = performance.now();
    var from = window.pageYOffset || document.documentElement.scrollTop,
      to = document.querySelector(sel).getBoundingClientRect().top;
    requestAnimationFrame(function step(timestamp) {
      var progress = (timestamp - start) / duration;
      1 <= progress && (progress = 1);
      window.scrollTo(0, (from + to * progress) | 0);
      1 > progress && (temp = requestAnimationFrame(step));
    });
  };
}
var scrollMenu = anim(1500);

// mobile menu
var openBtn = document.getElementById('view_menu');
var closeBtn = document.getElementById('close_menu');
var mobileMenu = document.getElementById('present__menu');

openBtn.addEventListener('click', function() {
  mobileMenu.style.display = 'block';
  closeBtn.style.display = 'block';
});
closeBtn.addEventListener('click', function() {
  mobileMenu.style.display = 'none';
  closeBtn.style.display = 'none';
});

// function handlerEmail(input) {
//     return function(event) {
//         event.preventDefault();
//         let inputValue = input.value;

//         if (inputValue === '' ) {
//             alert ('You should enter the e-mail')
//         } else {
//             let properties = {
//                 SecureToken : "3b367cf7-1f93-454c-856d-73021908a3d3",
//                 // To : 'ewgen15@gmail.com',
//                 To : 'h.zosimova@gmail.com',
//                 From : "agressicum@gmail.com",
//                 Subject : "New e-mail - new subscriber to updates!",
//                 Body : inputValue
//             }

//             Email.send(properties).then(message => {
//                 alert('You subscribed to our updates!')
//                 input.autofocus = false;
//                 input.value = '';
//             });
//         }
//     }

// }

// //contact
// var sendContact = document.getElementById('SendContact');
// var emailContactInput = document.getElementById('contactUsInput')

// //e-mail
// // var sendMessage = document.getElementById('SendEmail');
// // var emailInput = document.getElementById('joinUsInput')

// if (sendMessage) {
//     sendContact.addEventListener('click', handlerEmail(emailContactInput));
//     sendMessage.addEventListener('click', handlerEmail(emailInput));
// }
